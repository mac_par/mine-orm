/**
 * Core functionality package.
 * @see org.mineorm.core.ReflectionUtils
 */
package org.mineorm.core;
