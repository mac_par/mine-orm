package org.mineorm.core.jdbc.pool.config;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

import java.io.IOException;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;

/**
 * {@code ConfigDeserializer} provides configuration file deserialization logic.
 * Logic is based on <b>Jackson</b> library.
 */
public class ConfigDeserializer extends StdDeserializer<Config> {
    /**
     * Non-parameterized constructor, that is required by {@code StdDeserializer}.
     */
    public ConfigDeserializer() {
        super(Config.class);
    }

    /**
     * Deserialization method providing logic for conversion of xml configuration file into {@code Config} instance.
     *
     * @param jp                     Json Parser
     * @param deserializationContext Deserialization Context
     * @return initialized {@code Config} instance
     * @throws IOException             file-related issues
     * @throws JsonProcessingException json processing related issues
     */
    @Override
    public Config deserialize(JsonParser jp, DeserializationContext deserializationContext)
            throws IOException, JsonProcessingException {
        Properties props = new Properties();
        Set<String> packages = new HashSet<>();
        Set<String> entites = new HashSet<>();

        JsonToken jToken;
        String name = null;
        boolean isName = false;
        boolean isValue = false;
        boolean isClass = false;
        boolean isPackage = false;
        while ((jToken = jp.nextToken()) != null) {

            if (jToken == JsonToken.FIELD_NAME) {
                if ("name".equals(jp.getValueAsString())) {
                    isName = true;
                }

                if ("value".equals(jp.getValueAsString())) {
                    isValue = true;
                }
                if ("class".equals(jp.getValueAsString())) {
                    isClass = true;
                }
                if ("package".equals(jp.getValueAsString())) {
                    isPackage = true;
                }
            } else if (jToken == JsonToken.VALUE_STRING) {
                if (isName) {
                    name = jp.getValueAsString();
                    isName = false;
                }

                if (isValue) {
                    props.setProperty(name, jp.getValueAsString());
                    name = null;
                    isValue = false;
                }

                if (isClass) {
                    entites.add(jp.getValueAsString());
                    isClass = false;
                }

                if (isPackage) {
                    packages.add(jp.getValueAsString());
                    isPackage = false;
                }
            }
        }

        return new Config(props, entites, packages);
    }
}
