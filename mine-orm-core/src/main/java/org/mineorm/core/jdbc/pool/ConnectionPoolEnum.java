package org.mineorm.core.jdbc.pool;

import org.mineorm.core.jdbc.DBConnectionPool;

import java.util.function.Function;

/**
 * Enumerates available implementations of {@code DBConnectionPool} interface.
 */
public enum ConnectionPoolEnum {
    /**
     * HikariCP connection pool.
     */
    HIKARI_CONNECTION_POOL(HikariConnectionPool.class, HikariConnectionPool::new);

    private final Class<? extends DBConnectionPool> poolClass;
    private final Function<ConnectionPoolConfig, ? extends DBConnectionPool> constructor;

    ConnectionPoolEnum(Class<? extends DBConnectionPool> clazz,
                       Function<ConnectionPoolConfig, ? extends DBConnectionPool> creator) {
        this.poolClass = clazz;
        this.constructor = creator;
    }

    /**
     * Initialized specified Connection Pool with file-based configuration.
     *
     * @param config connection pool configuration
     * @return instance of {@code DBConnectionPool}
     */
    public DBConnectionPool createPool(ConnectionPoolConfig config) {
        return constructor.apply(config);
    }

    /**
     * Retrieves {@code ConnectionPoolEnum} by {@code DBConnectionPool} implementation class.
     *
     * @param clazz {@code Class<? extends DBConnectionPool>}looked connection pool implementation class
     * @return initialized connection pool
     * @throws ConfigurationException is thrown when looked implementation does not exist
     */
    public static ConnectionPoolEnum getConnectionPool(Class<?> clazz) {
        for (ConnectionPoolEnum inst : values()) {
            if (inst.poolClass.equals(clazz)) {
                return inst;
            }
        }

        throw new ConfigurationException(String.format("Class '%s' could not be found", clazz.getName()));
    }
}
