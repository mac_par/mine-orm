/**
 * Pool connection-related package.
 *
 * @see org.mineorm.core.jdbc.pool.AbstractDBConnectionPool
 * @see org.mineorm.core.jdbc.pool.ConfigProperties
 * @see org.mineorm.core.jdbc.pool.ConfigurationException
 * @see org.mineorm.core.jdbc.pool.ConnectionPoolConfig
 * @see org.mineorm.core.jdbc.pool.ConnectionPoolEnum
 *
 * <b>Connection pool implementations:</b>
 * @see org.mineorm.core.jdbc.pool.HikariConnectionPool
 */
package org.mineorm.core.jdbc.pool;
