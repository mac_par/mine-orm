package org.mineorm.core.jdbc.pool;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sql.DataSource;
import java.lang.invoke.MethodHandles;

/**
 * Abstract class for connection pool implementation with general functionality provided for further extension.
 */
abstract class AbstractDBConnectionPool {
    private static final Logger LOGGER = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
    private final DataSource dataSource;

    AbstractDBConnectionPool(ConnectionPoolConfig config) {
        this.dataSource = configure(config);
        logInfo("Data Source was initialized");
    }

    protected abstract DataSource configure(ConnectionPoolConfig config);

    protected void logInfo(String msg, Object... args) {
        LOGGER.info(msg, args);
    }

    protected void logWarn(String msg, Object... args) {
        LOGGER.warn(msg, args);
    }

    protected void logWarn(String msg) {
        LOGGER.warn(msg);
    }

    protected void logWarn(String msg, Throwable throwable) {
        LOGGER.warn(msg, throwable);
    }

    protected DataSource getDataSource() {
        return dataSource;
    }
}
