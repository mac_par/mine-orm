package org.mineorm.core.jdbc.pool.config;

import java.util.Properties;
import java.util.Set;

/**
 * Represents configuration file with all its properties for {@code Entity Manager}.
 */
public class Config {
    private final Properties properties;
    private final Set<String> entities;
    private final Set<String> packages;

    /**
     * Parameterized constructor with {@code java.util.Properties},
     *
     * @param props    container for main properties
     * @param classes  a set of entity classes from configuration file
     * @param packages a set of packages from configuration file
     */
    Config(Properties props, Set<String> classes, Set<String> packages) {
        properties = props;
        entities = classes;
        this.packages = packages;
    }

    /**
     * Retrieves main configuration properties.
     *
     * @return {@code java.util.Properties} instance
     */
    public Properties getProperties() {
        return properties;
    }

    /**
     * Retrieves a set with entities class paths from configuration file.
     *
     * @return {@code java.util.Set} with Strings
     */
    public Set<String> getEntities() {
        return entities;
    }

    /**
     * Retrieves a set with packages path containing entities.
     *
     * @return {@code java.util.Set} with Strings
     */
    public Set<String> getPackages() {
        return packages;
    }
}

