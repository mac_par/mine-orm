package org.mineorm.core.jdbc;

import org.mineorm.annotations.Entity;
import org.mineorm.core.jdbc.pool.ConnectionPoolConfig;
import org.mineorm.validation.ValidationException;
import org.mineorm.validation.WellformedEntityValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.invoke.MethodHandles;

/**
 * {@code EntityManager} is responsible for managing persistence of entities.
 */
public class EntityManager {
    private static final Logger LOGGER = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    /**
     * Creates {@code EntityManager} using configuration default mineorm-config.xml file.
     *
     * @return {@code EntityManager} instance
     */
    public static EntityManager getEntityManager() {
        ConnectionPoolConfig config = ConnectionPoolConfig.getConfiguration();

        validateEntities(config);
        return new EntityManager(config);
    }

    /**
     * Creates {@code EntityManager} using specified configuration file path.
     *
     * @param path configuration path
     * @return {@code EntityManager} instance
     */
    public static EntityManager getEntityManager(String path) {
        ConnectionPoolConfig config = ConnectionPoolConfig.getConfiguration(path);

        validateEntities(config);
        return new EntityManager(config);
    }

    /**
     * Package private parameterized constructor with {@code ConnectionPoolConfig}.
     *
     * @param config configuration
     */
    EntityManager(ConnectionPoolConfig config) {
        configureConnectionManager(config);
    }

    private static void validateEntities(ConnectionPoolConfig config) {
        WellformedEntityValidator validator = new WellformedEntityValidator();

        for (Class clazz : config.getEntites()) {
            if (clazz.isAnnotationPresent(Entity.class)) {
                if (!validator.validate(clazz)) {
                    LOGGER.warn("validateEntities - validation failed");
                    throw new ValidationException(validator.getViolationMessage());
                }
            } else {
                LOGGER.warn("validateEntities - {} is not an entity", clazz.getName());
                throw new ValidationException(
                        String.format("'%s' is not annotated with is not an entity class.", clazz.getName()));
            }
        }
    }

    private void configureConnectionManager(ConnectionPoolConfig config) {
        ConnectionManager.configureConnectionManager(config);
    }
}
