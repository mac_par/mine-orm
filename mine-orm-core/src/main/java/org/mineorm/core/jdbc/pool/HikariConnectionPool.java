package org.mineorm.core.jdbc.pool;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.mineorm.core.jdbc.DBConnectionPool;

import javax.sql.DataSource;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Objects;

/**
 * Connection pool based on HikariCP library.
 */
public final class HikariConnectionPool extends AbstractDBConnectionPool implements DBConnectionPool {
    /**
     * Parameterized constructor with {@code ConnectionPoolConfig}.
     * @param config configuration properties
     */
    HikariConnectionPool(ConnectionPoolConfig config) {
        super(config);
    }

    /**
     * Configures pool to retrieve {@code DataSource}.
     * @param config configuration
     * @return {@code DataSource} instance
     */
    @Override
    protected HikariDataSource configure(ConnectionPoolConfig config) {

        HikariConfig configuration = new HikariConfig();
        configuration.setDriverClassName(config.getDialect().driverClassName());
        configuration.setJdbcUrl(config.getJdbcUrl());
        configuration.setUsername(config.getUser());
        configuration.setPassword(config.getPassword());
        configuration.setPassword(config.getPassword());
        configuration.setConnectionTimeout(config.getTimeout());
        configuration.setIdleTimeout(config.getIdleTimeout());
        configuration.setMinimumIdle(config.getMinPoolSize());
        configuration.setMaximumPoolSize(config.getMaxPoolSize());
        configuration.setAutoCommit(config.isAutoComit());
        if (!Objects.isNull(config.getPoolName())) {
            configuration.setPoolName(config.getPoolName());
        }

        return new HikariDataSource(configuration);
    }

    /**
     * Obtains connection from connection pool.
     * @return {@code java.sql.Connection} instance
     * @throws SQLException occurs in case of any connection-related issues
     */
    @Override
    public Connection obtainConnection() throws SQLException {
        logInfo("Obtaining data source");
        return getDataSource().getConnection();
    }

    /**
     * Provides aut-closeable functionality for pool connection.
     * {@see java.io.Closeable}
     * @throws IOException whether any connection-related issue has occurred
     */
    @Override
    public void close() throws IOException {
        DataSource dataSource = getDataSource();
        if (!Objects.isNull(dataSource)) {
            ((HikariDataSource) dataSource).close();
            logInfo("Connection Pool was closed");
        }
    }
}
