package org.mineorm.core.jdbc.pool;

import java.util.Objects;
import java.util.Properties;

/**
 * Configuration file metadata, that provides the way to extract proper form of result data.
 */
public enum ConfigProperties {
    /**
     * Database dialect.
     */
    DIALECT_CLASS_NAME("mineorm.jdbc.dialect", true),

    /**
     * Pool class implementation.
     */
    POOL_CLASS_NAME("mineorm.pool.pool_class_name", true),

    /**
     * Jdbc connection url.
     */
    URL("mineorm.jdbc.url", true),

    /**
     * Username.
     */
    USER("mineorm.jdbc.username", true),

    /**
     * Password.
     */
    PASSWORD("mineorm.jdbc.password", true),

    /**
     * Auto-commit.
     */
    AUTO_COMMIT("mineorm.jdbc.auto_commit", false),

    /**
     * Connection timeout.
     */
    TIMEOUT("mineorm.pool.connection_timeout", false),

    /**
     * Maximal connection lifetime.
     */
    MAX_LIFETIME("mineorm.pool.max_lifetime", false),

    /**
     * Idle connection timeout.
     */
    IDLE_TIMEOUT("mineorm.pool.idle_timeout", false),

    /**
     * Connection pool minimal size.
     */
    MIN_POOL_SIZE("mineorm.pool.min_size", false),

    /**
     * Connection pool maximal size.
     */
    MAX_POOL_SIZE("mineorm.pool.max_size", false),

    /**
     * Connection pool name.
     */
    POOL_NAME("mineorm.pool.name", false);

    private static final String EXCEPTION_TEMPLATE = "Property '%s' is required";
    private String property;
    private boolean propertyRequired;

    ConfigProperties(String propertyField, boolean required) {
        this.property = propertyField;
        this.propertyRequired = required;
    }

    String extractPropertyValueAsString(Properties properties) {
        String value = properties.getProperty(getProperty());
        requireNotNull(value);

        return value;
    }

    Long extractPropertyValueAsLong(Properties properties) {
        Long value = null;
        try {
            value = Long.valueOf(extractPropertyValueAsString(properties));
        } catch (NumberFormatException ex) {
            //no action
        }
        return value;
    }

    Integer extractPropertyValueAsInteger(Properties properties) {
        Integer value = null;
        try {
            value = Integer.valueOf(extractPropertyValueAsString(properties));
        } catch (NumberFormatException ex) {
            //no action
        }
        return value;
    }

    Boolean extractPropertyValueAsBoolean(Properties properties) {
        return Boolean.valueOf(extractPropertyValueAsString(properties));
    }

    private String getProperty() {
        return property;
    }

    void requireNotNull(String str) {
        if (propertyRequired) {
            final String information = String.format(EXCEPTION_TEMPLATE, getProperty());
            Objects.requireNonNull(str, information);
            if (str.isBlank()) {
                throw new IllegalArgumentException(information);
            }
        }
    }
}
