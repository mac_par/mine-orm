package org.mineorm.core.jdbc;

/**
 * {@code MineOrmException} represents exceptions that may occur during execution of MineOrm.
 */
public class MineOrmException extends RuntimeException {
    /**
     * Parameterized constructor with String representing exception message.
     * @param message a short description of issue
     */
    public MineOrmException(String message) {
        super(message);
    }

    /**
     * Parameterized constructor with exception cause.
     * @param cause {@code Throwable} instance
     */
    public MineOrmException(Throwable cause) {
        super(cause);
    }
}
