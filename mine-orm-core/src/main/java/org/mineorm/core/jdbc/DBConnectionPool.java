package org.mineorm.core.jdbc;

import java.io.Closeable;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * Provides general Api for connection pool implementation.
 * Any implementation should reuse AbstractConnectionPool class.
 */
public interface DBConnectionPool extends Closeable {
    /**
     * Retrieves {@code java.sql.Connection} from connection pool.
     * @return {@code java.sql.Connection} instance
     * @throws SQLException for any problem with pool
     */
    Connection obtainConnection() throws SQLException;
}
