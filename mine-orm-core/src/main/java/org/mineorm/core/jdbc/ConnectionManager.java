package org.mineorm.core.jdbc;

import org.mineorm.core.jdbc.pool.ConfigurationException;
import org.mineorm.core.jdbc.pool.ConnectionPoolConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Objects;
import java.util.concurrent.Semaphore;

/**
 * Connection Manager is a singleton responsible for creation of connection pool
 * and retrieval of {@code java.sql.Connection} instances.
 */
final class ConnectionManager {
    private static final Logger LOGGER = LoggerFactory.getLogger(ConnectionManager.class);
    private static final Semaphore SEMAPHORE = new Semaphore(1);

    private static DBConnectionPool connectionPool;

    private ConnectionManager() {

    }

    /**
     * Obtains connection from underlying Database connection configuration.
     * @param config Configuration specifying the type of connection pool
     * @throws ConfigurationException may be thrown in the case of any configuration-related issue
     */
    static void configureConnectionManager(ConnectionPoolConfig config) {
        try {
            SEMAPHORE.acquire();
            if (Objects.isNull(connectionPool)) {

                connectionPool = config.getConnectionPool().createPool(config);
            }
        } catch (InterruptedException e) {
            LOGGER.warn("configureConnectionManager", e);
            throw new ConfigurationException(e);
        } finally {
            SEMAPHORE.release();
        }
    }

    /**
     * Obtains connection from underlying connection pool.
     * @return a {@code Connection} instance
     */
    static Connection obtainConnection() {
        try {
            SEMAPHORE.acquire();
            return connectionPool.obtainConnection();
        } catch (InterruptedException | SQLException e) {
            LOGGER.warn("configureConnectionManager", e);
            throw new MineOrmException(e);
        } finally {
            SEMAPHORE.release();
        }
    }
    /**
     * Closes Database connection pool.
     */
    static void close() {
        try {
            SEMAPHORE.acquire();
            if (!Objects.isNull(connectionPool)) {
                connectionPool.close();
                connectionPool = null;
                LOGGER.info("Connection pool was closed.");
            }
        } catch (InterruptedException | IOException e) {
            LOGGER.warn("Something happend during #close execution", e);
        } finally {
            SEMAPHORE.release();
        }
    }
}
