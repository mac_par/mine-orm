/**
 * Jdbc-related functionality package.
 * @see org.mineorm.core.jdbc.ConnectionManager
 * @see org.mineorm.core.jdbc.DBConnectionPool
 */
package org.mineorm.core.jdbc;
