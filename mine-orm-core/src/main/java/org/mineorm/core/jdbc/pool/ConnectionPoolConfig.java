package org.mineorm.core.jdbc.pool;

import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.mineorm.annotations.Entity;
import org.mineorm.core.ReflectionUtils;
import org.mineorm.core.dialect.DatabaseDialect;
import org.mineorm.core.jdbc.pool.config.Config;
import org.mineorm.core.jdbc.pool.config.ConfigDeserializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.util.HashSet;
import java.util.Objects;
import java.util.Properties;
import java.util.Set;

/**
 * Connection Pool Config encloses configuration for connection pool initialization.
 * Configuration is taken from 'mineorm-config.properties' file that undergoes further processing
 * in ConnectionPoolConfigBuilder.
 */
public final class ConnectionPoolConfig {
    private static final Logger LOGGER = LoggerFactory.getLogger(ConnectionPoolConfig.class);
    private static final String FILE_NAME = "mineorm-config.xml";

    private final DatabaseDialect dialect;
    private final ConnectionPoolEnum connectionPool;
    private final String jdbcUrl;
    private final String user;
    private final String password;
    private final String poolName;
    private final boolean isAutoComit;
    private final long timeout;
    private final long maxLifetime;
    private final long idleTimeout;
    private final int minPoolSize;
    private final int maxPoolSize;
    private final Set<Class> entites;


    private ConnectionPoolConfig(ConnectionPoolConfigBuilder builder) {
        this.dialect = builder.dialect;
        this.connectionPool = builder.connectionPool;
        this.jdbcUrl = builder.jdbcUrl;
        this.user = builder.user;
        this.password = builder.password;
        this.poolName = builder.poolName;
        this.isAutoComit = builder.isAutoComit;
        this.timeout = builder.timeout;
        this.maxLifetime = builder.maxLifetime;
        this.idleTimeout = builder.idleTimeout;
        this.minPoolSize = builder.minPoolSize;
        this.maxPoolSize = builder.maxPoolSize;
        this.entites = builder.entites;
    }

    /**
     * Prepares Connection pool configuration on the basis of mineorm-config.properties file.
     *
     * @return initialized configuration
     */
    public static ConnectionPoolConfig getConfiguration() {
        try {
            Config config = readConfigFile(FILE_NAME);
            return initializeBuilder(config).build();
        } catch (URISyntaxException | IOException | ClassNotFoundException | XMLStreamException e) {
            LOGGER.warn("getConfiguration", e);
            throw new ConfigurationException(e.getMessage());
        }
    }

    /**
     * Prepares Connection pool configuration on the basis of Mine Orm file passed in parameter.
     *
     * @param filePath path to file
     * @return initialized configuration
     */
    public static ConnectionPoolConfig getConfiguration(String filePath) {
        try {
            Config config = readConfigFile(filePath);
            return initializeBuilder(config).build();
        } catch (URISyntaxException | IOException | ClassNotFoundException | XMLStreamException e) {
            LOGGER.warn("getConfiguration", e);
            throw new ConfigurationException(e.getMessage());
        }


    }

    private static Config readConfigFile(String filePath) throws IOException, URISyntaxException, XMLStreamException {
        try (BufferedReader br = new BufferedReader(
                new FileReader(
                        Path.of(getConfigurationFile(filePath)).toFile(), StandardCharsets.UTF_8))) {
            XMLInputFactory factory = XMLInputFactory.newFactory();
            XMLStreamReader xmlStreamReader = factory.createXMLStreamReader(br);
            XmlMapper mapper = new XmlMapper();
            SimpleModule module = new SimpleModule();
            module.addDeserializer(Config.class, new ConfigDeserializer());
            mapper.registerModule(module);
            return mapper.readValue(xmlStreamReader, Config.class);
        }
    }

    /**
     * Retrievese Unified Resource Identifier using ClassLoader.
     *
     * @return resource URI
     * @throws URISyntaxException when URI is malformed
     */
    private static URI getConfigurationFile(String file) throws URISyntaxException {
        return Objects.requireNonNull(
                Thread.currentThread().getContextClassLoader().getResource(file)).toURI();
    }

    /**
     * Retrieves {@code DatabaseDialect} implementation.
     *
     * @return {@code DatabaseDialect} instance
     */
    public DatabaseDialect getDialect() {
        return dialect;
    }

    /**
     * Retrieves {@code ConnectionPoolEnum} instance.
     *
     * @return {@code ConnectionPoolEnum} instance
     */
    public ConnectionPoolEnum getConnectionPool() {
        return connectionPool;
    }

    /**
     * Retrieves string with database url.
     *
     * @return database url
     */
    public String getJdbcUrl() {
        return jdbcUrl;
    }

    /**
     * Retrieves username.
     *
     * @return username
     */
    public String getUser() {
        return user;
    }

    /**
     * Retrieves password.
     *
     * @return password
     */
    public String getPassword() {
        return password;
    }

    /**
     * Retrieves pool name string.
     *
     * @return pool name
     */
    public String getPoolName() {
        return poolName;
    }

    /**
     * Retrieves auto commit configuration.
     *
     * @return auto commit configuration
     */
    public boolean isAutoComit() {
        return isAutoComit;
    }

    /**
     * Retrieves amount of timeout.
     *
     * @return timeout
     */
    public long getTimeout() {
        return timeout;
    }

    /**
     * Retrieves amount of timeout.
     *
     * @return timeout
     */
    public long getMaxLifetime() {
        return maxLifetime;
    }

    /**
     * Retrieves idle timeout.
     *
     * @return idle timeout
     */
    public long getIdleTimeout() {
        return idleTimeout;
    }

    /**
     * Retrieves minimal connection pool size.
     *
     * @return minimal connection pool size
     */
    public int getMinPoolSize() {
        return minPoolSize;
    }

    /**
     * Retrieves maximal connection pool size.
     *
     * @return maximal connection pool size
     */
    public int getMaxPoolSize() {
        return maxPoolSize;
    }

    /**
     * Retrieves entity classes.
     *
     * @return {@code Set} of entity classes
     */
    public Set<Class> getEntites() {
        return entites;
    }

    private static ConnectionPoolConfigBuilder initializeBuilder(Config config)
            throws IOException, ClassNotFoundException {
        ConnectionPoolConfigBuilder builder = new ConnectionPoolConfigBuilder();

        populateBuilder(builder, config);

        return builder;
    }

    private static void populateBuilder(ConnectionPoolConfigBuilder builder, Config config)
            throws IOException, ClassNotFoundException {
        populateProperties(builder, config);
        populateEntities(builder, config);
    }

    private static void populateProperties(ConnectionPoolConfigBuilder builder, Config config) {
        final Properties properties = config.getProperties();
        final String dialectProperty = ConfigProperties.DIALECT_CLASS_NAME.extractPropertyValueAsString(properties);
        final DatabaseDialect dialect = ReflectionUtils.initializeInstanceFromPath(
                DatabaseDialect.class, dialectProperty);

        final String poolClassName = ConfigProperties.POOL_CLASS_NAME.extractPropertyValueAsString(properties);
        final Class<?> poolClass = ReflectionUtils.getClassByString(poolClassName);
        final ConnectionPoolEnum connectionPool = ConnectionPoolEnum.getConnectionPool(poolClass);

        final String jdbcURl = ConfigProperties.URL.extractPropertyValueAsString(properties);
        final String user = ConfigProperties.USER.extractPropertyValueAsString(properties);
        final String password = ConfigProperties.PASSWORD.extractPropertyValueAsString(properties);
        final Boolean autoCommit = ConfigProperties.AUTO_COMMIT.extractPropertyValueAsBoolean(properties);
        final Long timeout = ConfigProperties.TIMEOUT.extractPropertyValueAsLong(properties);
        final Long maxLifetime = ConfigProperties.MAX_LIFETIME.extractPropertyValueAsLong(properties);
        final Long idleTimeout = ConfigProperties.IDLE_TIMEOUT.extractPropertyValueAsLong(properties);
        final Integer minPoolSize = ConfigProperties.MIN_POOL_SIZE.extractPropertyValueAsInteger(properties);
        final Integer maxPoolSize = ConfigProperties.MAX_POOL_SIZE.extractPropertyValueAsInteger(properties);
        final String poolName = ConfigProperties.POOL_NAME.extractPropertyValueAsString(properties);

        builder.setDialect(dialect)
                .setConnectionPool(connectionPool)
                .setJdbcUrl(jdbcURl)
                .setUser(user)
                .setPassword(password)
                .setAutoComit(autoCommit)
                .setTimeout(timeout)
                .setMaxLifetime(maxLifetime)
                .setIdleTimeout(idleTimeout)
                .setMinPoolSize(minPoolSize)
                .setMaxPoolSize(maxPoolSize)
                .setPoolName(poolName);
    }

    private static void populateEntities(ConnectionPoolConfigBuilder builder, Config config)
            throws IOException, ClassNotFoundException {
        for (String clazz : config.getEntities()) {
            Class entity = ReflectionUtils.getClassByString(clazz);
            if (entity.isAnnotationPresent(Entity.class)) {
                builder.addEntity(entity);
            }
        }

        for (String packagePath : config.getPackages()) {
            for (Class clazz : ReflectionUtils.getEntitiesByPackage(packagePath)) {
                builder.addEntity(clazz);
            }
        }

    }

    private static class ConnectionPoolConfigBuilder {
        //a default connection timeout is 90 seconds
        private static long defaultConnectionTimeout = 90_000L;
        //a connection can be idle for 10 minuts max by default
        private static long defaultIdleConnectionTimeout = 600_000L;
        private static long defaultConnectionMaxLife = 1800_000L;
        private static int initialPoolSize = 10;

        private DatabaseDialect dialect;
        private ConnectionPoolEnum connectionPool;
        private String jdbcUrl;
        private String user;
        private String password;
        private String poolName;
        private boolean isAutoComit;
        private long timeout;
        private long idleTimeout;
        private long maxLifetime;
        private int minPoolSize;
        private int maxPoolSize;
        private final Set<Class> entites;

        ConnectionPoolConfigBuilder() {
            //auto commit is turned-on by default
            isAutoComit = true;
            timeout = defaultConnectionTimeout;
            idleTimeout = defaultIdleConnectionTimeout;
            maxLifetime = defaultConnectionMaxLife;
            minPoolSize = initialPoolSize;
            maxPoolSize = initialPoolSize;
            entites = new HashSet<>();
        }

        ConnectionPoolConfigBuilder setDialect(DatabaseDialect dialectInstance) {
            this.dialect = dialectInstance;
            return this;
        }

        ConnectionPoolConfigBuilder setConnectionPool(ConnectionPoolEnum cp) {
            this.connectionPool = cp;
            return this;
        }

        ConnectionPoolConfigBuilder setJdbcUrl(String url) {
            if (!Objects.isNull(url)) {
                this.jdbcUrl = url;
            }
            return this;
        }

        ConnectionPoolConfigBuilder setUser(String username) {
            if (!Objects.isNull(username)) {
                this.user = username;
            }

            return this;
        }

        ConnectionPoolConfigBuilder setPassword(String passwd) {
            if (!Objects.isNull(passwd)) {
                this.password = passwd;
            }

            return this;
        }

        ConnectionPoolConfigBuilder setPoolName(String pName) {
            if (!Objects.isNull(pName)) {
                this.poolName = pName;
            }

            return this;
        }

        ConnectionPoolConfigBuilder setAutoComit(Boolean autoComit) {
            if (!Objects.isNull(autoComit)) {
                isAutoComit = autoComit;
            }

            return this;
        }

        ConnectionPoolConfigBuilder setTimeout(Long lTimeout) {
            if (!Objects.isNull(lTimeout)) {
                this.timeout = lTimeout;
            }

            return this;
        }

        ConnectionPoolConfigBuilder setIdleTimeout(Long tValue) {
            if (!Objects.isNull(tValue)) {
                this.idleTimeout = tValue;
            }

            return this;
        }

        ConnectionPoolConfigBuilder setMaxLifetime(Long time) {
            if (!Objects.isNull(time)) {
                this.maxLifetime = time;
            }

            return this;
        }

        ConnectionPoolConfigBuilder setMinPoolSize(Integer poolSize) {
            if (!Objects.isNull(poolSize)) {
                this.minPoolSize = poolSize;
            }

            return this;
        }

        ConnectionPoolConfigBuilder setMaxPoolSize(Integer poolSize) {
            if (!Objects.isNull(poolSize)) {
                this.maxPoolSize = poolSize;
            }

            return this;
        }

        void addEntity(Class entityClass) {
            if (!Objects.isNull(entityClass)) {
                this.entites.add(entityClass);
            }
        }

        ConnectionPoolConfig build() {
            return new ConnectionPoolConfig(this);
        }


    }
}
