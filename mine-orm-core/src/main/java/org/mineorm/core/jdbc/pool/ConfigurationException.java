package org.mineorm.core.jdbc.pool;

/**
 * Class representing exceptions related to configuration of Entity Manager components.
 */
public class ConfigurationException extends RuntimeException {

    /**
     * Parameterized constructor with String.
     * @param message exception message
     */
    public ConfigurationException(String message) {
        super(message);
    }

    /**
     * Parameterized constructor with {@code Throwable}, that caused that exception.
     * @param cause a cause of exception
     */
    public ConfigurationException(Throwable cause) {
        super(cause);
    }
}
