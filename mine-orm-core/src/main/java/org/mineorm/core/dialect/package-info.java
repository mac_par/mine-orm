/**
 * Database dialects-related package.
 * @see org.mineorm.core.dialect.DatabaseDialect
 *
 * Implementations:
 * @see org.mineorm.core.dialect.H2Dialect
 */
package org.mineorm.core.dialect;
