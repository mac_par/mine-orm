package org.mineorm.core.dialect;

/**
 * Interface {@code DatabaseDialect} provides api for retrieving vendor-specific information reusable for proper
 * work with database.
 */
public interface DatabaseDialect {
    /**
     * Retrieves driver's source class implementation.
     * @return path to Vendor's {@code DataSource} class
     */
    String dataSourceClassName();

    /**
     * Retrieves driver class name.
     * @return driver class name
     */
    String driverClassName();
}
