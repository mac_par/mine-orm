package org.mineorm.core.dialect;

/**
 * Encloses H2 database-specific metadata.
 */
public final class H2Dialect implements DatabaseDialect {
    /**
     * Non-parametized constructor for Reflection purposes.
     */
    private H2Dialect() {

    }

    private static final String DATA_SOURCE_CLASS_NAME = "org.h2.jdbcx.JdbcDataSource";
    private static final String DRIVER_CLASS = "org.h2.Driver";

    /**
     * Retrieves H2 DataSource class name.
     * @return data source class name
     */
    @Override
    public String dataSourceClassName() {
        return DATA_SOURCE_CLASS_NAME;
    }

    /**
     * Retrieves H2 Driver class.
     * @return H2 Driver class
     */
    @Override
    public String driverClassName() {
        return DRIVER_CLASS;
    }
}
