package org.mineorm.core;

import org.mineorm.annotations.Entity;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * ReflectiveUtils class provides methods, that are commonly used in case of entities.
 */
public final class ReflectionUtils {
    private ReflectionUtils() {
    }


    private static final Pattern GENERIC_PATTERN = Pattern.compile("^[\\w.\\s]+<(.*?)>[\\w.\\s]*$");
    private static final String CLASS_EXTENSION = ".class";

    /**
     * Creates collection instance on the basis of field and input values.
     *
     * @param field  entity field annotated with {@code Collection} or relation indicating a collection.
     * @param values result values retrieved from data source
     * @param <T>    generic type of result collection and input values
     * @return whether provided field matches {@code java.util.List} or {@code java.util.Set}, passed values
     * are located in correct collection. Otherwise passed values are returned.
     */
    public static <T> Collection<T> createCollection(Field field, Collection<T> values) {
        if (List.class.equals(field.getType())) {
            return createList(values);
        } else if (Set.class.equals(field.getType())) {
            return createSet(values);
        }
        return values;
    }

    private static <T> List<T> createList(Collection<T> values) {
        return new ArrayList<>(values);
    }

    private static <T> Set<T> createSet(Collection<T> values) {
        return new HashSet<>(values);
    }

    /**
     * Extracts generic content type from field by the means of reflection.
     *
     * @param field generic field
     * @return {@code java.lang.Class} of generic content, otherwise {@code java.lang.Object} is returned
     */
    public static Class<?> extractGenericTypeContentByField(Field field) {
        Matcher matcher = GENERIC_PATTERN.matcher(field.getGenericType().getTypeName());
        if (matcher.matches()) {
            return getClassByString(matcher.group(1));
        }
        return Object.class;
    }

    /**
     * Creates {@code java.lang.Class} instance for passed string.
     * For well-formed string package path and existing {@code java.lang.Class} proper instance is returned.
     * Otherwise {@code java.lang.Class<Object>} is returned.
     *
     * @param path a string reflecting a path to {@code java.lang.Class} instance
     * @return instance of {@code java.lang.Class<?>}, where '?' is a proper class
     */
    public static Class<?> getClassByString(String path) {
        try {
            return Class.forName(path);
        } catch (ClassNotFoundException e) {
            throw new IllegalArgumentException(
                    String.format("Provided path class does not exists: '%s'", path), e);
        }
    }

    /**
     * On the basis of package path returns a table of classes located in that package and its sub-packages.
     *
     * @param packageName path to a certain package
     * @return returns a {@code java.lang.Class} array
     * @throws IOException            in case of any problem with files
     * @throws ClassNotFoundException in case that class does not exists
     */
    public static Class[] getClassesFromPackageHierarchy(String packageName)
            throws IOException, ClassNotFoundException {
        Iterable<File> files = retrieveFilesCollection(packageName);

        Set<Class<?>> classesSet = new HashSet<>();
        for (File file : files) {
            classesSet.addAll(getClassByFileAndPackage(file, packageName, true));
        }

        return classesSet.toArray(Class[]::new);
    }

    /**
     * Extracts classes from specified package.
     *
     * @param packageName package path
     * @return {@code Class} array
     * @throws IOException            on input exception
     * @throws ClassNotFoundException looked class was not found
     */
    public static Class[] getClassesByPackage(String packageName) throws IOException, ClassNotFoundException {
        Iterable<File> files = retrieveFilesCollection(packageName);

        Set<Class<?>> classesSet = new HashSet<>();

        for (File file : files) {
            classesSet.addAll(getClassByFileAndPackage(file, packageName, false));
        }

        return classesSet.toArray(Class[]::new);
    }

    /**
     * Extracts entity classes from specified package.
     *
     * @param packageName package path
     * @return {@code Class} array
     * @throws IOException            on input exception
     * @throws ClassNotFoundException looked class was not found
     */
    public static Class[] getEntitiesByPackage(String packageName) throws IOException, ClassNotFoundException {
        Iterable<File> files = retrieveFilesCollection(packageName);

        Set<Class<?>> classesSet = new HashSet<>();

        for (File file : files) {
            classesSet.addAll(getClassByFileAndPackage(file, packageName, false));
        }

        return classesSet.stream()
                .filter(ReflectionUtils::isEntity)
                .collect(Collectors.toSet()).toArray(Class[]::new);
    }

    private static Iterable<File> retrieveFilesCollection(String packageName) throws IOException {
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        final String path = packageName.replaceAll("\\.", File.separator);
        Enumeration<URL> enumeration = classLoader.getResources(path);

        List<File> resourcesFiles = new ArrayList<>();

        while (enumeration.hasMoreElements()) {
            resourcesFiles.add(new File(enumeration.nextElement().getFile()));
        }
        return resourcesFiles;
    }

    private static Set<Class<?>> getClassByFileAndPackage(File file, String packagePath,
                                                          boolean checkDirectories)
            throws ClassNotFoundException {

        Set<Class<?>> set = new HashSet<>();
        if (file.exists()) {
            File[] files = file.listFiles((dir, name) -> name.endsWith(CLASS_EXTENSION) || dir.isDirectory());
            for (File result : files) {
                if (result.isDirectory()) {
                    if (checkDirectories) {
                        set.addAll(getClassByFileAndPackage(result,
                                packagePath + "." + result.getName(), checkDirectories));
                    }
                } else {
                    String className = result.getName().substring(0, result.getName().indexOf(CLASS_EXTENSION));
                    String classPath = String.format("%s.%s", packagePath, className);
                    Class<?> clazz = Class.forName(classPath);
                    set.add(clazz);
                }
            }
        }

        return set;
    }

    private static boolean isEntity(Class<?> clazz) {
        return clazz.isAnnotationPresent(Entity.class);
    }

    /**
     * Performs a cast of a passed {@code java.lang.Object} into class specified by {@code java.lang.Class} instance.
     *
     * @param castClass target class for the cast
     * @param object    object instance to be casted
     * @param <T>       class generic type of result class
     * @return a requested class object or object of {@code java.lang.Object} when class could not be retireved
     * from class path
     */
    public static <T> T cast(Class<T> castClass, Object object) {
        return castClass.cast(object);
    }

    /**
     * Initializes an instance of a class by the means of passed class path and then casts it into requested
     * {@code java.lang.Class<T>}. Initialization is based on non-parameterized constructor.
     *
     * @param clazz     a {@code java.lang.Class<T>} to cast result object
     * @param classPath class path into requested class
     * @param <T>       generic type of result object
     * @return initialized object
     */
    public static <T> T initializeInstanceFromPath(Class<T> clazz, String classPath) {
        Object object;
        try {
            Constructor<?> constructor = getClassByString(classPath).getDeclaredConstructor();
            setAccessible(constructor);
            object = constructor.newInstance();
        } catch (NoSuchMethodException | IllegalAccessException
                | InstantiationException | InvocationTargetException ex) {
            throw new IllegalArgumentException(ex);
        }
        return cast(clazz, object);
    }

    /**
     * Changes {@code java.lang.reflect.Field} accessibility.
     *
     * @param field target method
     */
    public static void setAccessible(Field field) {
        if (!Objects.isNull(field) && isPrivateOrPackagePrivate(field.getModifiers())) {
            field.setAccessible(true);
        }
    }

    /**
     * Changes {@code java.lang.reflect.Method} accessibility.
     *
     * @param method target method
     */
    public static void setAccessible(Method method) {
        if (!Objects.isNull(method) && isPrivateOrPackagePrivate(method.getModifiers())) {
            method.setAccessible(true);
        }
    }

    /**
     * Changes {@code java.lang.reflect.Constructor} accessibility.
     *
     * @param constructor instance of constructor
     * @param <T>         generic constructor type
     */
    public static <T> void setAccessible(Constructor<T> constructor) {
        if (!Objects.isNull(constructor) && isPrivateOrPackagePrivate(constructor.getModifiers())) {
            constructor.setAccessible(true);
        }
    }

    /**
     * Checks whether passed modifiers indicates private or package private element.
     *
     * @param modifiers {@code int} representation of modifiers bit map
     * @return {@code true} whether modifier indicates private of package private modifier.
     * {@code false} otherwise.
     */
    private static boolean isPrivateOrPackagePrivate(int modifiers) {
        return Modifier.isPrivate(modifiers) || (!Modifier.isPublic(modifiers) && !Modifier.isProtected(modifiers));
    }
}
