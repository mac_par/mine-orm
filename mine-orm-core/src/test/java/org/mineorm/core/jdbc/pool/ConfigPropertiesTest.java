package org.mineorm.core.jdbc.pool;

import org.testng.annotations.Test;

import java.util.Properties;

import static org.assertj.core.api.Assertions.assertThat;

public class ConfigPropertiesTest {
    @Test(expectedExceptions = {NullPointerException.class},
            expectedExceptionsMessageRegExp = "Property 'mineorm.jdbc.dialect' is required")
    public void missingDialectPropertyRisesNullPointerException() {
        //When
        Properties props = new Properties();
        //Given
        //Then

        ConfigProperties.DIALECT_CLASS_NAME.extractPropertyValueAsString(props);
    }

    @Test(expectedExceptions = {IllegalArgumentException.class},
            expectedExceptionsMessageRegExp = "Property 'mineorm.jdbc.dialect' is required")
    public void blankDialectRisesIllegalArgumentException() {
        //When
        Properties props = new Properties();
        props.setProperty("mineorm.jdbc.dialect", "");
        //Given
        //Then

        ConfigProperties.DIALECT_CLASS_NAME.extractPropertyValueAsString(props);
    }

    @Test
    public void properDialectPropertyReturnsClass() {
        //When
        Properties props = new Properties();
        String value = "org.mineorm.core.dialect.H2Dialect";
        props.setProperty("mineorm.jdbc.dialect", value);
        //Given
        //Then

        String result = ConfigProperties.DIALECT_CLASS_NAME.extractPropertyValueAsString(props);
        assertThat(result).isNotNull();
        assertThat(result).isEqualTo(value);
    }

    @Test(expectedExceptions = {NullPointerException.class},
            expectedExceptionsMessageRegExp = "Property 'mineorm.pool.pool_class_name' is required")
    public void missingPoolClassNamePropertyRisesNullPointerException() {
        //When
        Properties props = new Properties();
        //Given
        //Then

        ConfigProperties.POOL_CLASS_NAME.extractPropertyValueAsString(props);
    }

    @Test(expectedExceptions = {IllegalArgumentException.class},
            expectedExceptionsMessageRegExp = "Property 'mineorm.pool.pool_class_name' is required")
    public void blanPoolClassNamePropertyRisesIllegalArgumentException() {
        //When
        Properties props = new Properties();
        props.setProperty("mineorm.pool.pool_class_name", "");
        //Given
        //Then

        ConfigProperties.POOL_CLASS_NAME.extractPropertyValueAsString(props);
    }

    @Test
    public void properPoolClassNamePropertyReturnsValue() {
        //When
        Properties props = new Properties();
        String value = "jakas klasa";
        props.setProperty("mineorm.pool.pool_class_name", value);
        //Given
        //Then

        String result = ConfigProperties.POOL_CLASS_NAME.extractPropertyValueAsString(props);
        assertThat(result).isNotNull();
        assertThat(result).isEqualTo(value);
    }

    @Test(expectedExceptions = {NullPointerException.class},
            expectedExceptionsMessageRegExp = "Property 'mineorm.jdbc.url' is required")
    public void missingUrlPropertyRisesNullPointerException() {
        //When
        Properties props = new Properties();
        //Given
        //Then

        ConfigProperties.URL.extractPropertyValueAsString(props);
    }

    @Test(expectedExceptions = {IllegalArgumentException.class},
            expectedExceptionsMessageRegExp = "Property 'mineorm.jdbc.url' is required")
    public void blankURLRisesIllegalArgumentException() {
        //When
        Properties props = new Properties();
        props.setProperty("mineorm.jdbc.url", "");
        //Given
        //Then

        ConfigProperties.URL.extractPropertyValueAsString(props);
    }

    @Test
    public void properUrlPropertyReturnsValue() {
        //When
        Properties props = new Properties();
        String value = "jakis url";
        props.setProperty("mineorm.jdbc.url", value);
        //Given
        //Then

        String result = ConfigProperties.URL.extractPropertyValueAsString(props);
        assertThat(result).isNotNull();
        assertThat(result).isEqualTo(value);
    }

    @Test(expectedExceptions = {NullPointerException.class},
            expectedExceptionsMessageRegExp = "Property 'mineorm.jdbc.username' is required")
    public void missingUserPropertyRisesNullPointerException() {
        //When
        Properties props = new Properties();
        //Given
        //Then

        ConfigProperties.USER.extractPropertyValueAsString(props);
    }

    @Test(expectedExceptions = {IllegalArgumentException.class},
            expectedExceptionsMessageRegExp = "Property 'mineorm.jdbc.username' is required")
    public void blankUSerRisesIllegalArgumentException() {
        //When
        Properties props = new Properties();
        props.setProperty("mineorm.jdbc.username", "");
        //Given
        //Then

        ConfigProperties.USER.extractPropertyValueAsString(props);
    }

    @Test
    public void properUserPropertyReturnsValue() {
        //When
        Properties props = new Properties();
        String value = "jakis url";
        props.setProperty("mineorm.jdbc.username", value);
        //Given
        //Then

        String result = ConfigProperties.USER.extractPropertyValueAsString(props);
        assertThat(result).isNotNull();
        assertThat(result).isEqualTo(value);
    }

    @Test(expectedExceptions = {NullPointerException.class},
            expectedExceptionsMessageRegExp = "Property 'mineorm.jdbc.password' is required")
    public void missingPasswordPropertyRisesNullPointerException() {
        //When
        Properties props = new Properties();
        //Given
        //Then

        ConfigProperties.PASSWORD.extractPropertyValueAsString(props);
    }

    @Test(expectedExceptions = {IllegalArgumentException.class},
            expectedExceptionsMessageRegExp = "Property 'mineorm.jdbc.password' is required")
    public void blankPasswordRisesIllegalArgumentException() {
        //When
        Properties props = new Properties();
        props.setProperty("mineorm.jdbc.password", "");
        //Given
        //Then

        ConfigProperties.PASSWORD.extractPropertyValueAsString(props);
    }

    @Test
    public void properPasswordPropertyReturnsValue() {
        //When
        Properties props = new Properties();
        String value = "jakis url";
        props.setProperty("mineorm.jdbc.password", value);
        //Given
        //Then

        String result = ConfigProperties.PASSWORD.extractPropertyValueAsString(props);
        assertThat(result).isNotNull();
        assertThat(result).isEqualTo(value);
    }

    @Test
    public void blankAutoCommitIsAllowed() {
        //When
        Properties props = new Properties();
        props.setProperty("mineorm.jdbc.auto_commit", "");
        //Given
        //Then

        String result = ConfigProperties.AUTO_COMMIT.extractPropertyValueAsString(props);
        assertThat(result).isBlank();
    }

    @Test
    public void missingAutoCommitIsAllowed() {
        //When
        Properties props = new Properties();
        //Given
        //Then

        String result = ConfigProperties.AUTO_COMMIT.extractPropertyValueAsString(props);
        assertThat(result).isNull();
    }

    @Test
    public void properAutocomitPropertyReturnsValue() {
        //When
        Properties props = new Properties();
        String value = "false";
        props.setProperty("mineorm.jdbc.auto_commit", value);
        //Given
        //Then

        String result = ConfigProperties.AUTO_COMMIT.extractPropertyValueAsString(props);
        assertThat(result).isNotNull();
        assertThat(result).isEqualTo(value);
    }

    @Test
    public void properAutocomitPropertyReturnsCorrectValue1() {
        //When
        Properties props = new Properties();
        String value = "false";
        props.setProperty("mineorm.jdbc.auto_commit", value);
        //Given
        //Then

        Boolean result = ConfigProperties.AUTO_COMMIT.extractPropertyValueAsBoolean(props);
        assertThat(result).isNotNull();
        assertThat(result).isFalse();
    }

    @Test
    public void properAutocomitPropertyReturnsCorrectValue2() {
        //When
        Properties props = new Properties();
        String value = "true";
        props.setProperty("mineorm.jdbc.auto_commit", value);
        //Given
        //Then

        Boolean result = ConfigProperties.AUTO_COMMIT.extractPropertyValueAsBoolean(props);
        assertThat(result).isNotNull();
        assertThat(result).isTrue();
    }

    @Test
    public void wrongAutocomitPropertyReturnsFalse() {
        //When
        Properties props = new Properties();
        String value = "sdfsdfs";
        props.setProperty("mineorm.jdbc.auto_commit", value);
        //Given
        //Then

        Boolean result = ConfigProperties.AUTO_COMMIT.extractPropertyValueAsBoolean(props);
        assertThat(result).isNotNull();
        assertThat(result).isFalse();
    }

    @Test
    public void blankAutocomitPropertyReturnsFalse() {
        //When
        Properties props = new Properties();
        String value = "";
        props.setProperty("mineorm.jdbc.auto_commit", value);
        //Given
        //Then

        Boolean result = ConfigProperties.AUTO_COMMIT.extractPropertyValueAsBoolean(props);
        assertThat(result).isNotNull();
        assertThat(result).isFalse();
    }

    @Test
    public void missingAutocomitPropertyReturnsFalse() {
        //When
        Properties props = new Properties();
        //Given
        //Then

        Boolean result = ConfigProperties.AUTO_COMMIT.extractPropertyValueAsBoolean(props);
        assertThat(result).isNotNull();
        assertThat(result).isFalse();
    }

    @Test
    public void blankTimeoutIsAllowed() {
        //When
        Properties props = new Properties();
        props.setProperty("mineorm.pool.connection_timeout", "");
        //Given
        //Then

        String result = ConfigProperties.TIMEOUT.extractPropertyValueAsString(props);
        assertThat(result).isBlank();
    }

    @Test
    public void missingTimeoutIsAllowed() {
        //When
        Properties props = new Properties();
        //Given
        //Then

        String result = ConfigProperties.TIMEOUT.extractPropertyValueAsString(props);
        assertThat(result).isNull();
    }

    @Test
    public void properTimeoutPropertyReturnsValue() {
        //When
        Properties props = new Properties();
        String value = "false";
        props.setProperty("mineorm.pool.connection_timeout", value);
        //Given
        //Then

        String result = ConfigProperties.TIMEOUT.extractPropertyValueAsString(props);
        assertThat(result).isNotNull();
        assertThat(result).isEqualTo(value);
    }

    @Test
    public void properTimeoutPropertyReturnsALong() {
        //When
        Properties props = new Properties();
        String value = "2";
        props.setProperty("mineorm.pool.connection_timeout", value);
        //Given
        //Then

        Object result = ConfigProperties.TIMEOUT.extractPropertyValueAsLong(props);
        assertThat(result).isNotNull();
        assertThat(result).isInstanceOf(Long.class);
    }

    @Test
    public void wrongTimeoutPropertyReturnsNull() {
        //When
        Properties props = new Properties();
        String value = "L2";
        props.setProperty("mineorm.pool.connection_timeout", value);
        //Given
        //Then

        Object result = ConfigProperties.TIMEOUT.extractPropertyValueAsLong(props);
        assertThat(result).isNull();
    }

    @Test
    public void blankIdleTimeoutIsAllowed() {
        //When
        Properties props = new Properties();
        props.setProperty("mineorm.pool.idle_timeout", "");
        //Given
        //Then

        String result = ConfigProperties.IDLE_TIMEOUT.extractPropertyValueAsString(props);
        assertThat(result).isBlank();
    }

    @Test
    public void missingIdleTimeoutIsAllowed() {
        //When
        Properties props = new Properties();
        //Given
        //Then

        String result = ConfigProperties.IDLE_TIMEOUT.extractPropertyValueAsString(props);
        assertThat(result).isNull();
    }

    @Test
    public void properIdleTimeoutPropertyReturnsValue() {
        //When
        Properties props = new Properties();
        String value = "false";
        props.setProperty("mineorm.pool.idle_timeout", value);
        //Given
        //Then

        String result = ConfigProperties.IDLE_TIMEOUT.extractPropertyValueAsString(props);
        assertThat(result).isNotNull();
        assertThat(result).isEqualTo(value);
    }

    @Test
    public void properIdleTimeoutPropertyReturnsaLong() {
        //When
        Properties props = new Properties();
        String value = "15";
        props.setProperty("mineorm.pool.idle_timeout", value);
        //Given
        //Then

        Object result = ConfigProperties.IDLE_TIMEOUT.extractPropertyValueAsLong(props);
        assertThat(result).isNotNull();
        assertThat(result).isInstanceOf(Long.class);
    }

    @Test
    public void wrongIdleTimeoutPropertyReturnsNull() {
        //When
        Properties props = new Properties();
        String value = "15L";
        props.setProperty("mineorm.pool.idle_timeout", value);
        //Given
        //Then

        Object result = ConfigProperties.IDLE_TIMEOUT.extractPropertyValueAsLong(props);
        assertThat(result).isNull();
    }

    @Test
    public void blankMinPoolSizeIsAllowed() {
        //When
        Properties props = new Properties();
        props.setProperty("mineorm.pool.minSize", "");
        //Given
        //Then

        String result = ConfigProperties.MIN_POOL_SIZE.extractPropertyValueAsString(props);
        assertThat(result).isBlank();
    }

    @Test
    public void missingMinPoolSizeIsAllowed() {
        //When
        Properties props = new Properties();
        //Given
        //Then

        String result = ConfigProperties.MIN_POOL_SIZE.extractPropertyValueAsString(props);
        assertThat(result).isNull();
    }

    @Test
    public void properMinPoolSizePropertyReturnsValue() {
        //When
        Properties props = new Properties();
        String value = "false";
        props.setProperty("mineorm.pool.min_size", value);
        //Given
        //Then

        String result = ConfigProperties.MIN_POOL_SIZE.extractPropertyValueAsString(props);
        assertThat(result).isNotNull();
        assertThat(result).isEqualTo(value);
    }

    @Test
    public void properMinPoolSizePropertyReturnsAnInteger() {
        //When
        Properties props = new Properties();
        String value = "1";
        props.setProperty("mineorm.pool.min_size", value);
        //Given
        //Then

        Object result = ConfigProperties.MIN_POOL_SIZE.extractPropertyValueAsInteger(props);
        assertThat(result).isNotNull();
        assertThat(result).isInstanceOf(Integer.class);
    }

    @Test
    public void wrongMinPoolSizePropertyReturnsNull() {
        //When
        Properties props = new Properties();
        String value = "dfs";
        props.setProperty("mineorm.pool.min_size", value);
        //Given
        //Then

        Object result = ConfigProperties.MIN_POOL_SIZE.extractPropertyValueAsInteger(props);
        assertThat(result).isNull();
    }

    @Test
    public void blankMaxPoolSizeIsAllowed() {
        //When
        Properties props = new Properties();
        props.setProperty("mineorm.pool.max_size", "");
        //Given
        //Then

        String result = ConfigProperties.MAX_POOL_SIZE.extractPropertyValueAsString(props);
        assertThat(result).isBlank();
    }

    @Test
    public void missingMaxPoolSizeIsAllowed() {
        //When
        Properties props = new Properties();
        //Given
        //Then

        String result = ConfigProperties.MAX_POOL_SIZE.extractPropertyValueAsString(props);
        assertThat(result).isNull();
    }

    @Test
    public void properMaxPoolSizePropertyReturnsValue() {
        //When
        Properties props = new Properties();
        String value = "false";
        props.setProperty("mineorm.pool.max_size", value);
        //Given
        //Then

        String result = ConfigProperties.MAX_POOL_SIZE.extractPropertyValueAsString(props);
        assertThat(result).isNotNull();
        assertThat(result).isEqualTo(value);
    }

    @Test
    public void properMaxPoolSizePropertyReturnsNullOnWrongValue() {
        //When
        Properties props = new Properties();
        String value = "false";
        props.setProperty("mineorm.pool.max_size", value);
        //Given
        //Then

        Object result = ConfigProperties.MAX_POOL_SIZE.extractPropertyValueAsInteger(props);
        assertThat(result).isNull();
    }

    @Test
    public void properMaxPoolSizePropertyReturnsAnInteger() {
        //When
        Properties props = new Properties();
        String value = "15";
        props.setProperty("mineorm.pool.max_size", value);
        //Given
        //Then

        Object result = ConfigProperties.MAX_POOL_SIZE.extractPropertyValueAsInteger(props);
        assertThat(result).isNotNull();
        assertThat(result).isInstanceOf(Integer.class);
    }

    @Test
    public void blankPoolNameIsAllowed() {
        //When
        Properties props = new Properties();
        props.setProperty("mineorm.pool.name", "");
        //Given
        //Then

        String result = ConfigProperties.POOL_NAME.extractPropertyValueAsString(props);
        assertThat(result).isBlank();
    }

    @Test
    public void missingPoolNameIsAllowed() {
        //When
        Properties props = new Properties();
        //Given
        //Then

        String result = ConfigProperties.POOL_NAME.extractPropertyValueAsString(props);
        assertThat(result).isNull();
    }

    @Test
    public void properPoolNamePropertyReturnsValue() {
        //When
        Properties props = new Properties();
        String value = "false";
        props.setProperty("mineorm.pool.name", value);
        //Given
        //Then

        String result = ConfigProperties.POOL_NAME.extractPropertyValueAsString(props);
        assertThat(result).isNotNull();
        assertThat(result).isEqualTo(value);
    }

    @Test
    public void properPoolNamePropertyIsAString() {
        //When
        Properties props = new Properties();
        String value = "cos";
        props.setProperty("mineorm.pool.name", value);
        //Given
        //Then

        Object result = ConfigProperties.POOL_NAME.extractPropertyValueAsString(props);
        assertThat(result).isNotNull();
        assertThat(result).isEqualTo(value);
        assertThat(result).isInstanceOf(String.class);
    }

    @Test
    public void missingMaxLifetimeIsNotAProblem() {
        //When
        Properties props = new Properties();
        //Given
        //Then

        Long value = ConfigProperties.MAX_LIFETIME.extractPropertyValueAsLong(props);
        assertThat(value).isNull();
    }

    @Test
    public void emptyMaxLifetimePropertyReturnsNull() {
        //When
        Properties props = new Properties();
        props.setProperty("mineorm.pool.max_lifetime", "");
        //Given
        //Then

        Long result = ConfigProperties.MAX_LIFETIME.extractPropertyValueAsLong(props);
        assertThat(result).isNull();
    }

    @Test
    public void wrongMaxLifetimePropertyReturnsNull() {
        //When
        Properties props = new Properties();
        props.setProperty("mineorm.pool.max_lifetime", "13l");
        //Given
        //Then

        Long result = ConfigProperties.MAX_LIFETIME.extractPropertyValueAsLong(props);
        assertThat(result).isNull();
    }

    @Test
    public void properValueOfMaxLifetimeMustBeReturned() {
        //When
        Properties props = new Properties();
        Long value = 13L;
        props.setProperty("mineorm.pool.max_lifetime", value.toString());
        //Given
        //Then

        Long result = ConfigProperties.MAX_LIFETIME.extractPropertyValueAsLong(props);
        assertThat(result).isNotNull();
        assertThat(result).isEqualTo(value);
    }
}