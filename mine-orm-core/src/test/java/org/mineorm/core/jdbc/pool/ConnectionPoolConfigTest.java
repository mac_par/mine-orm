package org.mineorm.core.jdbc.pool;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class ConnectionPoolConfigTest {
    public static final String path = "mineorm-config.xml";
    private static ConfigBox configBox;

    @BeforeClass
    public static void setUp() {
        configBox = new ConfigBox();
        configBox.readPropertiesFromFile(path);
    }

    @AfterClass
    public static void tearDown() {
        configBox = null;
    }

    @Test
    public void whenDefaultFileIsUsedPropertiesMustBeFoundAndMatch() {
        ConnectionPoolConfig config = ConnectionPoolConfig.getConfiguration();
        doesPropertiesMatch(config);
    }

    @Test
    public void whenUserDefinedFileIsUsedPropertiesMustBeFoundAndMatch() {
        ConnectionPoolConfig config = ConnectionPoolConfig.getConfiguration("mineorm-config.xml");
        doesPropertiesMatch(config);
    }

    private void doesPropertiesMatch(ConnectionPoolConfig config) {
        assertThat(config.getDialect().getClass()).isEqualTo(configBox.getDialect().getClass());
        assertThat(config.getConnectionPool()).isEqualTo(configBox.getConnectionPool());
        assertThat(config.getJdbcUrl()).isEqualTo(configBox.getUrl());
        assertThat(config.getUser()).isEqualTo(configBox.getUser());
        assertThat(config.getPassword()).isEqualTo(configBox.getPassword());
        assertThat(config.getTimeout()).isEqualTo(configBox.getTimeout());
        assertThat(config.getMaxLifetime()).isEqualTo(configBox.getMaxLifetime());
        assertThat(config.isAutoComit()).isEqualTo(configBox.getAutoCommit());
        assertThat(config.getIdleTimeout()).isEqualTo(configBox.getIdleTimeout());
        assertThat(config.getMinPoolSize()).isEqualTo(configBox.getMinSize());
        assertThat(config.getMaxPoolSize()).isEqualTo(configBox.getMaxSize());
        assertThat(config.getPoolName()).isEqualTo(configBox.getPoolName());
        assertThat(config.getEntites()).isEqualTo(configBox.getEntites());
    }

}