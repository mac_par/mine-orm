package org.mineorm.core.jdbc;

import org.mineorm.validation.ValidationException;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.*;

public class EntityManagerTest {
    @Test
    public void entityManagerUsingDefaultFileConfMustPass() {
        //When
        //Given
        EntityManager em = EntityManager.getEntityManager();
        //Then

        assertThat(em).isNotNull();
    }

    @Test(expectedExceptions = {ValidationException.class},
            expectedExceptionsMessageRegExp = "Class org.mineorm.core.entites.wrong.WrongEntity " +
                    "does not have serialVersionUID or is malformed\n")
    public void entityManagerUsingWrongEntityShouldThrowAnException() {
        //When
        //Given
        //Then

        assertThat(EntityManager.getEntityManager("mineorm-wrongent-config.xml"))
                .withFailMessage("Class [a-zA-Z.]* does not have serialVersionUID or is malformed");
    }

    @Test(expectedExceptions = {ValidationException.class},
            expectedExceptionsMessageRegExp = "Class org.mineorm.core.packages.wrong.Entity4 does not implement hashCode/equals methods\n")
    public void entityManagerUsingWrongPackageShouldThrowAnException() {
        //When
        //Given
        //Then

        assertThat(EntityManager.getEntityManager("mineorm-wrongpack-config.xml"))
                .withFailMessage("Class [a-zA-Z.]* does not implement hashCode/equals methods\n");
    }
}