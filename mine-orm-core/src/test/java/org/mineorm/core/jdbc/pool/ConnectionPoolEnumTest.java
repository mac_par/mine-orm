package org.mineorm.core.jdbc.pool;

import org.mineorm.core.jdbc.DBConnectionPool;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.*;

public class ConnectionPoolEnumTest {

    @Test(expectedExceptions = {ConfigurationException.class},
            expectedExceptionsMessageRegExp = "Class 'org.mineorm.core.jdbc.DBConnectionPool' could not be found")
    public void lookedClassDoesNotExist() {
        //When
        //Given
        Class<? extends DBConnectionPool> clazz = DBConnectionPool.class;
        //Then

        ConnectionPoolEnum.getConnectionPool(clazz);
    }

    @Test
    public void hikariPoolCanBeExtracted() {
        //When
        //Given
        Class<? extends DBConnectionPool> clazz = HikariConnectionPool.class;
        //Then

        ConnectionPoolEnum enumInst = ConnectionPoolEnum.getConnectionPool(clazz);
        assertThat(enumInst).isEqualTo(ConnectionPoolEnum.HIKARI_CONNECTION_POOL);
    }

    @Test
    public void hikariPoolCanBeInitialized() {
        //When
        //Given
        Class<? extends DBConnectionPool> clazz = HikariConnectionPool.class;
        //Then

        ConnectionPoolEnum enumInst = ConnectionPoolEnum.getConnectionPool(clazz);
        DBConnectionPool pool = enumInst.createPool(ConnectionPoolConfig.getConfiguration());
        assertThat(pool).isNotNull();
        assertThat(pool).isInstanceOf(HikariConnectionPool.class);
    }
}