package org.mineorm.core.jdbc.pool;

import org.mineorm.core.jdbc.DBConnectionPool;
import org.testng.annotations.Test;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import static org.assertj.core.api.Assertions.*;

public class HikariConnectionPoolTest {
    @Test
    public void connectionPoolShouldBeCreatable() throws IOException {
        //When
        ConnectionPoolConfig config = ConnectionPoolConfig.getConfiguration();
        //Given
        //Then
        DBConnectionPool pool = config.getConnectionPool().createPool(config);

        assertThat(pool).isNotNull();
        assertThat(pool).isInstanceOf(HikariConnectionPool.class);
        pool.close();
    }

    @Test
    public void connectionShouldBeRetievable() throws IOException {
        //When
        ConnectionPoolConfig config = ConnectionPoolConfig.getConfiguration();
        DBConnectionPool pool = config.getConnectionPool().createPool(config);
        //Given
        //Then

        try (Connection connection = pool.obtainConnection()) {
            assertThat(connection).isNotNull();
        } catch (SQLException e) {
            fail(e.getMessage());
        }

        pool.close();
    }
}