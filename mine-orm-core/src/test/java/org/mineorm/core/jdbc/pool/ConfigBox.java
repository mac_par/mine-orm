package org.mineorm.core.jdbc.pool;

import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.mineorm.annotations.Entity;
import org.mineorm.core.ReflectionUtils;
import org.mineorm.core.dialect.DatabaseDialect;
import org.mineorm.core.jdbc.pool.config.Config;
import org.mineorm.core.jdbc.pool.config.ConfigDeserializer;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.util.HashSet;
import java.util.Objects;
import java.util.Properties;
import java.util.Set;

class ConfigBox {
    //a default connection timeout is 30 seconds
    private static long DEFAULT_CONNECTION_TIMEOUT = 30_000L;
    //a connection can be idle for 10 minuts max by default
    private static long DEFAULT_IDLE_CONNECTION_TIMEOUT = 600_000L;
    private static long DEFAULT_MAX_LIFETIME = 1800_000L;
    private static int INITIAL_POOL_SIZE = 10;

    private DatabaseDialect dialect;
    private ConnectionPoolEnum connectionPool;
    private String url;
    private String user;
    private String password;
    private String poolName;
    private Boolean autoCommit;
    private Long timeout = DEFAULT_CONNECTION_TIMEOUT;
    private Long maxLifetime = DEFAULT_MAX_LIFETIME;
    private Long idleTimeout = DEFAULT_IDLE_CONNECTION_TIMEOUT;
    private Integer minSize = INITIAL_POOL_SIZE;
    private Integer maxSize = INITIAL_POOL_SIZE;
    private final Set<Class> entites = new HashSet<>();

    public ConfigBox() {
    }

    public void readPropertiesFromFile(String path) {

        try (BufferedReader br = new BufferedReader(
                new FileReader(
                        Path.of(getURI(path)).toFile(), StandardCharsets.UTF_8))) {
            XMLInputFactory factory = XMLInputFactory.newFactory();
            XMLStreamReader xmlStreamReader = factory.createXMLStreamReader(br);
            XmlMapper mapper = new XmlMapper();
            SimpleModule module = new SimpleModule();
            module.addDeserializer(Config.class, new ConfigDeserializer());
            mapper.registerModule(module);
            Config config = mapper.readValue(xmlStreamReader, Config.class);
            populate(config);
        } catch (XMLStreamException | IOException | URISyntaxException | ClassNotFoundException e) {
            throw new ConfigurationException(e.getMessage());
        }
    }

    private URI getURI(String filePath) throws URISyntaxException {
        return Objects.requireNonNull(Thread.currentThread()
                .getContextClassLoader().getResource(filePath)).toURI();
    }

    private void populate(Config config) throws IOException, ClassNotFoundException {
        final Properties properties = config.getProperties();
        final String dialectProperty = ConfigProperties.DIALECT_CLASS_NAME.extractPropertyValueAsString(properties);
        setDialect(ReflectionUtils.initializeInstanceFromPath(DatabaseDialect.class, dialectProperty));
        Class<?> poolClass = ReflectionUtils.getClassByString(
                ConfigProperties.POOL_CLASS_NAME.extractPropertyValueAsString(properties));
        setConnectionPool(ConnectionPoolEnum.getConnectionPool(poolClass));

        setUrl(ConfigProperties.URL.extractPropertyValueAsString(properties));
        setUser(ConfigProperties.USER.extractPropertyValueAsString(properties));
        setPassword(ConfigProperties.PASSWORD.extractPropertyValueAsString(properties));
        setAutoCommit(ConfigProperties.AUTO_COMMIT.extractPropertyValueAsBoolean(properties));
        setTimeout(ConfigProperties.TIMEOUT.extractPropertyValueAsLong(properties));
        setMaxLifetime(ConfigProperties.MAX_LIFETIME.extractPropertyValueAsLong(properties));
        setIdleTimeout(ConfigProperties.IDLE_TIMEOUT.extractPropertyValueAsLong(properties));
        setMinSize(ConfigProperties.MIN_POOL_SIZE.extractPropertyValueAsInteger(properties));
        setMaxSize(ConfigProperties.MAX_POOL_SIZE.extractPropertyValueAsInteger(properties));
        setPoolName(ConfigProperties.POOL_NAME.extractPropertyValueAsString(properties));

        for (String clazz : config.getEntities()) {
            Class entity = ReflectionUtils.getClassByString(clazz);
            if (entity.isAnnotationPresent(Entity.class)) {
                addEntity(entity);
            }
        }

        for (String packagePath : config.getPackages()) {
            for (Class clazz : ReflectionUtils.getEntitiesByPackage(packagePath)) {
                addEntity(clazz);
            }
        }
    }

    public DatabaseDialect getDialect() {
        return dialect;
    }

    public ConnectionPoolEnum getConnectionPool() {
        return connectionPool;
    }

    public String getUrl() {
        return url;
    }

    public String getUser() {
        return user;
    }

    public String getPassword() {
        return password;
    }

    public String getPoolName() {
        return poolName;
    }

    public Boolean getAutoCommit() {
        return autoCommit;
    }

    public Long getTimeout() {
        return timeout;
    }

    public Long getMaxLifetime() {
        return maxLifetime;
    }

    public Long getIdleTimeout() {
        return idleTimeout;
    }

    public Integer getMinSize() {
        return minSize;
    }

    public Integer getMaxSize() {
        return maxSize;
    }

    public Set<Class> getEntites() {
        return entites;
    }

    void setDialect(DatabaseDialect dialect) {
        if (!Objects.isNull(dialect)) {
            this.dialect = dialect;
        }
    }

    void setConnectionPool(ConnectionPoolEnum connectionPool) {
        if (!Objects.isNull(connectionPool)) {
            this.connectionPool = connectionPool;
        }
    }

    void setUrl(String url) {
        if (!Objects.isNull(url)) {
            this.url = url;
        }
    }

    void setUser(String user) {
        if (!Objects.isNull(user)) {
            this.user = user;
        }
    }

    void setPassword(String password) {
        if (!Objects.isNull(password)) {
            this.password = password;
        }
    }

    void setPoolName(String poolName) {
        if (!Objects.isNull(poolName)) {
            this.poolName = poolName;
        }
    }

    void setAutoCommit(Boolean autoCommit) {
        if (!Objects.isNull(autoCommit)) {
            this.autoCommit = autoCommit;
        }
    }

    void setTimeout(Long timeout) {
        if (!Objects.isNull(timeout)) {
            this.timeout = timeout;
        }
    }

    void setMaxLifetime(Long time) {
        if (!Objects.isNull(time)) {
            this.maxLifetime = time;
        }
    }

    void setIdleTimeout(Long idleTimeout) {
        if (!Objects.isNull(idleTimeout)) {
            this.idleTimeout = idleTimeout;
        }
    }

    void setMinSize(Integer minSize) {
        if (!Objects.isNull(minSize)) {
            this.minSize = minSize;
        }
    }

    void setMaxSize(Integer maxSize) {
        if (!Objects.isNull(maxSize)) {
            this.maxSize = maxSize;
        }
    }

    void addEntity(Class entityClass) {
        if (!Objects.isNull(entityClass)) {
            this.entites.add(entityClass);
        }
    }
}
