package org.mineorm.annotations.access;

/**
 * Specifies access mode into entity's content.
 * Available accesses: by field or method.
 */
public enum AccessMode {
    /**
     * Reflective access by field name.
     */
    FIELD,
    /**
     * Reflective access by method name.
     * Setters and getters should by convention start with set/get and field name.
     */
    METHOD,
}
