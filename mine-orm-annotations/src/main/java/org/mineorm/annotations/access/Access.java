package org.mineorm.annotations.access;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotation specifies access to entity's content by means of {@code AccessMode} enum.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
@Documented
public @interface Access {
    /**
     * Specifies the way entity's content is accessed.
     * By default reflection uses field names.
     * @return instance of {@code AccessMode}
     */
    AccessMode value() default AccessMode.FIELD;
}
