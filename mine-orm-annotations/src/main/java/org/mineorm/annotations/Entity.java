package org.mineorm.annotations;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * {@code Entity} annotation describes essential information on entity.
 * Requirements: a non-parameterized constructor, {@code java.lang.Serializable} marker interface,
 * serialVersionUID variable.
 */

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
@Documented
public @interface Entity {
    /**
     * Contains entity's name. When empty class name is taken.
     * @return entity's name
     */
    String name() default "";
}
