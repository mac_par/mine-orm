package org.mineorm.annotations;

import org.mineorm.annotations.generation.GenerationType;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * {@code GenerationMode} specifies the way id is generated.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD, ElementType.METHOD})
@Documented
public @interface GenerationMode {
    /**
     * Returns an instance of {@code GenerationType}, that reflects id generation method.
     * @return generation type
     */
    GenerationType type();
}
