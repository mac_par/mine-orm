package org.mineorm.annotations;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * {@code Table} annotation provides description on table name, schema etc.
 */

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
@Documented
public @interface Table {
    /**
     * Provides data regards table name.
     * When no value is provided annotated class simple name is taken.
     * @return table name
     */
    String name() default "";

    /**
     * Provides data on schema name for contained base.
     * When both name and schema are provided data will be used in form 'schema.name'.
     * @return table schema
     */
    String schema() default "";
}
