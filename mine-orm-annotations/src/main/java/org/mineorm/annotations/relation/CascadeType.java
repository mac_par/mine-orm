package org.mineorm.annotations.relation;

/**
 * Enum specifies type of actions that can be cascaded..
 */
public enum CascadeType {
    /**
     * Save action.
     */
    SAVE,

    /**
     * Update action.
     */
    UPDATE,

    /**
     * Removal action.
     */
    DELETE,

    /**
     * All actions are to be cascaded.
     */
    ALL,
}
