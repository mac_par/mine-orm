package org.mineorm.annotations.relation;

import org.mineorm.annotations.Column;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 *{@code Collection} annotation indicates a collection of elements in form of {@code java.util.List} or
 * {@code java.util.Set} with a class with at least <u>non-parametrized constructor</u>.
 * This annotation provides information for extraction of collection of elements from the same table as main entity.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD, ElementType.METHOD})
@Documented
public @interface Collection {
    /**
     * Details regards column, where elements are located.
     * @return {@code org.mineorm.annotation.Column} instance
     */
    Column column();
    /**
     * Defines fetch type for the collection.
     * By default eager mode is used.
     * @return {@code FetchType} instance
     */
    FetchType fetch() default FetchType.EAGER;

    /**
     * Order in which collection is organised.
     * By default ascending order is used.
     * @return instance of {@code org.mineorm.annotation.relation.OrderType}
     */
    OrderType order() default OrderType.ASC;
}
