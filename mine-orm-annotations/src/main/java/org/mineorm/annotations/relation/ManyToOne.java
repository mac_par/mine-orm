package org.mineorm.annotations.relation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 *Annotation specifies details regards Many-to-One relation.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD, ElementType.METHOD})
@Documented
public @interface ManyToOne {
    /**
     * Details regards join for Many-To-One relation.
     *
     * @return join details.
     */
    JoinColumn join();

    /**
     * Defines fetch type for the relation.
     * By default eager mode is used.
     *
     * @return {@code FetchType} instance
     */
    FetchType fetch() default FetchType.EAGER;

    /**
     * Indicates what type of actions can be performed on this relation.
     *
     * @return list of allowed actions.
     */
    CascadeType[] cascade() default {};
}
