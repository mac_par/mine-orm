package org.mineorm.annotations.relation;

/**
 * Enum specifies whether element should be retrieved by default
 * or not.
 */
public enum FetchType {
    /**
     * Lazy - An element should not be retrieved from database upon request.
     */
    LAZY,
    /**
     * Eager - an element must be retrieved from database along with entity.
     */
    EAGER,
}
