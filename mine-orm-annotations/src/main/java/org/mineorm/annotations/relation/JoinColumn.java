package org.mineorm.annotations.relation;

import org.mineorm.annotations.Column;
import org.mineorm.annotations.Table;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Reflects join relation between two tables.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD, ElementType.METHOD})
@Documented
public @interface JoinColumn {
    /**
     * Source column details for join.
     *
     * @return column name
     */
    Column source();

    /**
     * Specifies join table.
     *
     * @return table details
     */
    Table table();

    /**
     * Specifies details regards join column.
     *
     * @return column details
     */
    Column destination();

    /**
     * Specifies join type. By default left join is set.
     *
     * @return {@code java.lang.annotation.relation.JoinType} instance
     */
    JoinType join() default JoinType.LEFT;
}
