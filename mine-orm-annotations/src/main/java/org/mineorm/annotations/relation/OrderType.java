package org.mineorm.annotations.relation;

/**
 * Specifies the way elements are ordered in collection.
 */
public enum OrderType {
    /**
     * Ascending order.
     */
    ASC,

    /**
     * Descending order.
     */
    DESC,
}
