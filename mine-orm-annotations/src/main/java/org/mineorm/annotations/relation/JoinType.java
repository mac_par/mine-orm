package org.mineorm.annotations.relation;

/**
 * Indicates a join type.
 */
public enum JoinType {
    /**
     * Left join.
     */
    LEFT,

    /**
     * Right join.
     */
    RIGHT,

    /**
     * Inner join.
     */
    INNER,

    /**
     * Full join.
     */
    FULL
}
