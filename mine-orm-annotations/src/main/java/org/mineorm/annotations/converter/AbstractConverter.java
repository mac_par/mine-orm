package org.mineorm.annotations.converter;

/**
 * {@code AbstractConverter} provides the means of transforming an object into object from package {@code java.sql},
 * and in other way.
 * @param <F> source type
 * @param <T> target type
 */
public abstract class AbstractConverter<F, T> {
    /**
     * Converts source type into target type.
     * @param obj source type
     * @return result
     */
    public abstract T convertTo(F obj);

    /**
     * Converts target type into source type.
     * @param obj target type
     * @return result
     */
    public abstract F convertFrom(T obj);
}
