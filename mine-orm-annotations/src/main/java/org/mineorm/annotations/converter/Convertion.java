package org.mineorm.annotations.converter;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Provides the means of converting an item into item from {@code java.sql}.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD, ElementType.FIELD})
@Documented
public @interface Convertion {
    /**
     * {@code java.lang.Class} to converter class.
     * @return converter instance
     */
    Class<? extends AbstractConverter> value();
}
