package org.mineorm.annotations;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotation specifies column details related to annotated field/method.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD, ElementType.METHOD})
@Documented
public @interface Column {
    /**
     * Database column name mapped to this component.
     * @return column name
     */
    String name();

    /**
     *Specifies whether value can be updated.
     * @return {@code true} when can be updated in database(read-only), {@code false} otherwise
     */
    boolean updateable() default true;
}
