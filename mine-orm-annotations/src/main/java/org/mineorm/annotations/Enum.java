package org.mineorm.annotations;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Defines database representation of {@code Enum<E>} instance.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD, ElementType.METHOD})
@Documented
public @interface Enum {
    /**
     * Defines a way of representing enum in database.
     * Ordinal is a default representation of enum instances.
     * @return {@code EnumValues} instance
     */
    EnumValues representation() default EnumValues.ORDINAL;

    /**
     * Specifies whether annotated column must have a value.
     * By default true is returned.
     * @return {@code true} when value is required, {@code false} otherwise
     */
    boolean required() default true;
}
