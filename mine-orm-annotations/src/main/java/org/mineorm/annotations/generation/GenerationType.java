package org.mineorm.annotations.generation;

/**
 * Enum representing supported id generation methods.
 */
public enum GenerationType {
    /**
     * Auto-increment field. No actions required except retrieval of key.
     */
    IDENTITY,

    /**
     * Sequence is countable for id generation.
     */
    SEQUENCE,
}
