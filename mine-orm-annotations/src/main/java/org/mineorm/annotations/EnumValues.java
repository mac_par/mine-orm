package org.mineorm.annotations;

/**
 * Defines Enum representation.
 */
public enum EnumValues {
    /**
     * Represents an enum instance as a value string.
     */
    VALUE,

    /**
     * Represents an enum instance as an ordinal.
     */
    ORDINAL,
}
