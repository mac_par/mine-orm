package org.mineorm.annotations;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Specifies a sequence used for id generation.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD, ElementType.METHOD})
@Documented
public @interface SequenceGenerator {
    /**
     * Provides sequence name.
     * @return sequence name.
     */
    String sequence();

    /**
     * Provides database schema details if available.
     * When both sequence and schema are provided a designated sequence is requested by 'schema.sequence'.
     * @return schema name
     */
    String schema() default "";
}
