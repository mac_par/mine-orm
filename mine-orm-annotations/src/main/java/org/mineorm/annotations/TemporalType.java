package org.mineorm.annotations;

/**
 * {@code org.mineorm.annotations.TemporalType} represents the forms
 * that Time/Date can be represented.
 */

public enum TemporalType {
    /**
     *In form of Date in analogy to {@code java.sql.Timestamp}.
     */
    TIMESTAMP,

    /**
     *In form of Date in analogy to {@code java.sql.Date}.
     */
    DATE,

    /**
     *In form of Time in analogy to {@code java.sql.Time}.
     */
    TIME,
}
