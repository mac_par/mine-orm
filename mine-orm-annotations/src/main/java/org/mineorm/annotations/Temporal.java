package org.mineorm.annotations;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Provides data about the form date/time-related items should be kept in database.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD, ElementType.METHOD})
@Documented
public @interface Temporal {
    /**
     * Temporal representation.
     *
     * @return instance of {@code TemporalType} enum
     */
    TemporalType type();
}
