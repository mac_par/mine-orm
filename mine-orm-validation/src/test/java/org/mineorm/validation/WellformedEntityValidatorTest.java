package org.mineorm.validation;

import org.testng.annotations.Test;

import java.io.Serializable;

import static org.assertj.core.api.Assertions.*;

public class WellformedEntityValidatorTest {
    @Test
    public void classWithoutNonParameterizedConstructorMustFail() {
        //When
        WellformedEntityValidator validator = new WellformedEntityValidator();
        //Given
        final Class clazz = NoConstructor.class;
        //Then
        final String expectedMsg = String.format("Class %s does not implement non-parameterized constructor%n"
                , clazz.getName());
        assertThat(validator.validate(clazz)).isFalse();
        assertThat(validator.getViolationMessage()).isEqualTo(expectedMsg);
    }

    @Test
    public void classParameterizedConstructorMustPass() {
        //When
        WellformedEntityValidator validator = new WellformedEntityValidator();
        //Given
        final Class clazz = WithConstructor.class;
        final String expectedMsg = String.format("Class %s does not implement java.io.Serializable interface%n"
                , clazz.getName());
        //Then
        assertThat(validator.validate(clazz)).isFalse();
        assertThat(validator.getViolationMessage()).isEqualTo(expectedMsg);
    }
    @Test
    public void notSerializableObjectMustFail() {
        //When
        WellformedEntityValidator validator = new WellformedEntityValidator();
        //Given
        final Class clazz = NonSerializableClass.class;
        //Then
        final String expectedMsg = String.format("Class %s does not implement java.io.Serializable interface%n"
                , clazz.getName());
        assertThat(validator.validate(clazz)).isFalse();
        assertThat(validator.getViolationMessage()).isEqualTo(expectedMsg);
    }

    @Test
    public void missingSerialVersionUIDCausesFailure() {
        //When
        WellformedEntityValidator validator = new WellformedEntityValidator();
        //Given
        final Class clazz = SerializableNoUIDClass.class;
        //Then
        final String expectedMsg = String.format("Class %s does not have serialVersionUID or is malformed%n"
                , clazz.getName());
        assertThat(validator.validate(clazz)).isFalse();
        assertThat(validator.getViolationMessage()).isEqualTo(expectedMsg);
    }

    @Test
    public void malformedSerialVersionUIDCausesFailure1() {
        //When
        WellformedEntityValidator validator = new WellformedEntityValidator();
        //Given
        final Class clazz = MalformedSerializableUIDClass1.class;
        //Then
        final String expectedMsg = String.format("Class %s does not have serialVersionUID or is malformed%n"
                , clazz.getName());
        assertThat(validator.validate(clazz)).isFalse();
        assertThat(validator.getViolationMessage()).isEqualTo(expectedMsg);
    }

    @Test
    public void malformedSerialVersionUIDCausesFailure2() {
        //When
        WellformedEntityValidator validator = new WellformedEntityValidator();
        //Given
        final Class clazz = MalformedSerializableUIDClass2.class;
        //Then
        final String expectedMsg = String.format("Class %s does not have serialVersionUID or is malformed%n"
                , clazz.getName());
        assertThat(validator.validate(clazz)).isFalse();
        assertThat(validator.getViolationMessage()).isEqualTo(expectedMsg);
    }

    @Test
    public void malformedSerialVersionUIDCausesFailure3() {
        //When
        WellformedEntityValidator validator = new WellformedEntityValidator();
        //Given
        final Class clazz = MalformedSerializableUIDClass3.class;
        //Then
        final String expectedMsg = String.format("Class %s does not have serialVersionUID or is malformed%n"
                , clazz.getName());
        assertThat(validator.validate(clazz)).isFalse();
        assertThat(validator.getViolationMessage()).isEqualTo(expectedMsg);
    }

    @Test
    public void missingHashCodeCausesToFail() {
        //When
        WellformedEntityValidator validator = new WellformedEntityValidator();
        //Given
        final Class clazz = MissingHashCodeClass3.class;
        //Then
        final String expectedMsg = String.format("Class %s does not implement hashCode/equals methods%n"
                , clazz.getName());
        assertThat(validator.validate(clazz)).isFalse();
        assertThat(validator.getViolationMessage()).isEqualTo(expectedMsg);
    }

    @Test
    public void missingEqualsCausesToFail() {
        //When
        WellformedEntityValidator validator = new WellformedEntityValidator();
        //Given
        final Class clazz = MissingEqualsClass3.class;
        //Then
        final String expectedMsg = String.format("Class %s does not implement hashCode/equals methods%n"
                , clazz.getName());
        assertThat(validator.validate(clazz)).isFalse();
        assertThat(validator.getViolationMessage()).isEqualTo(expectedMsg);
    }

    @Test
    public void wellformedClassMustBeVictorious() {
        //When
        WellformedEntityValidator validator = new WellformedEntityValidator();
        //Given
        final Class object = ClassMurattbe.class;
        //Then
        assertThat(validator.validate(object)).isTrue();
        assertThat(validator.getViolationMessage()).isEmpty();
        assertThat(validator.getViolationMessage()).isBlank();
    }

    private static class NonSerializableClass {
        public NonSerializableClass() {
        }
    }
    private static class NoConstructor {

    }

    private static class WithConstructor {
        public WithConstructor() {
        }
    }
    private static class SerializableNoUIDClass implements Serializable {
        public SerializableNoUIDClass() {
        }
    }
    private static class MalformedSerializableUIDClass1 implements Serializable {
        private final Long serialU = 1L;

        public MalformedSerializableUIDClass1() {
        }
    }
    private static class MalformedSerializableUIDClass2 implements Serializable {
        private final Long serialVersionUID = 1L;

        public MalformedSerializableUIDClass2() {
        }
    }

    private static class MalformedSerializableUIDClass3 implements Serializable {
        private static final int serialVersionUID = 1;

        public MalformedSerializableUIDClass3() {
        }
    }

    private static class MissingHashCodeClass3 implements Serializable {
        private static final long serialVersionUID = 1L;

        public MissingHashCodeClass3() {
        }
    }

    private static class MissingEqualsClass3 implements Serializable {
        private static final long serialVersionUID = 1;

        public MissingEqualsClass3() {
        }

        @Override
        public int hashCode() {
            return super.hashCode();
        }
    }

    private static class ClassMurattbe implements Serializable {
        private static final Long serialVersionUID = 1L;

        public ClassMurattbe() {
        }

        @Override
        public int hashCode() {
            return super.hashCode();
        }

        @Override
        public boolean equals(Object obj) {
            return super.equals(obj);
        }
    }
}