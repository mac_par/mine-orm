package org.mineorm.validation;

import org.mineorm.validation.checker.ConstraintViolationCheck;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;

/**
 * {@code WellformedEntityValidator} checks entity alignment with required entity structure.
 * <ol>
 * <li>Has non-parameterized constructor</li>
 * <li>Entity implements {@code java.io.Serializable marker interface}</li>
 * <li>Entity contains a field with serialVersionUID: a private, final static long field</li>
 * <li>Entity has both {@code hashCode} and {@code equals} method implemented at class level</li>
 * </ol>
 */
public class WellformedEntityValidator extends ConstraintViolationCheck<Object, Class> {
    private static final String SERIAL_VERSION_UID = "serialVersionUID";
    private static final String SERIALIZABLE_VIOLATION = "Class %s does not implement java.io.Serializable interface";
    private static final String SVUID_VIOLATION = "Class %s does not have %s or is malformed";
    private static final String HASHCODE_EQUALS_VIOLATION = "Class %s does not implement hashCode/equals methods";
    private static final String CONSTRUCTOR_VIOLATION = "Class %s does not implement non-parameterized constructor";
    private static final String HASH_CODE_METHOD = "hashCode";
    private static final String EQUALS_METHOD = "equals";

    /**
     * Non-parameterized constructor.
     *
     */
    public WellformedEntityValidator() {
        super(null);
    }

    /**
     * Validates an entity for serialization, serialVersionUID field,
     * and both {@code hashCode()} and {@code equals()} methods to be overriden.
     * @param clazz entity class.
     * @return {@code true} on successfull validation. {@code false} otherwise.
     */
    @Override
    public final boolean validate(Class clazz) {
        clear();
        return validateObject(clazz);
    }

    private boolean validateObject(Class clazz) {
        Object instance;
        try {
            instance = clazz.getDeclaredConstructor().newInstance();
        } catch (IllegalAccessException | InstantiationException
                | NoSuchMethodException | InvocationTargetException e) {
            addViolation(String.format(CONSTRUCTOR_VIOLATION, clazz.getName()));
            return false;
        }

        return checkSerializable(instance, clazz) && checkSerialVersionUIDField(clazz)
               && checkHashCodeAndEquals(clazz);
    }

    private boolean checkSerializable(Object object, Class<?> clazz) {
        if (!(object instanceof Serializable)) {
            addViolation(String.format(SERIALIZABLE_VIOLATION, clazz.getName()));
            return false;
        }

        return true;
    }

    private boolean checkSerialVersionUIDField(Class<?> clazz) {
        boolean result = false;
        try {
            Field field = clazz.getDeclaredField(SERIAL_VERSION_UID);
            int modifiers = field.getModifiers();

            result = Modifier.isPrivate(modifiers)
                    && Modifier.isFinal(modifiers)
                    && Modifier.isStatic(modifiers)
                    && (field.getType().equals(long.class) || field.getType().equals(Long.class));
        } catch (NoSuchFieldException e) {
            //left empty on purpose
        }

        if (!result) {
            addViolation(String.format(SVUID_VIOLATION, clazz.getName(), SERIAL_VERSION_UID));
        }

        return result;
    }

    private boolean checkHashCodeAndEquals(Class<?> clazz) {
        try {
            clazz.getDeclaredMethod(HASH_CODE_METHOD);
            clazz.getDeclaredMethod(EQUALS_METHOD, Object.class);
            return true;
        } catch (NoSuchMethodException e) {
            addViolation(String.format(HASHCODE_EQUALS_VIOLATION, clazz.getName()));
            return false;
        }
    }
}
