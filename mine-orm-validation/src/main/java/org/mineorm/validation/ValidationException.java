package org.mineorm.validation;

/**
 * {@code ValidationException} signals incorrect entity state on any stage of its lifecycle.
 * Also provides crucial pieces of information for diagnostic reasons in form a user defined message,
 * that is favourable over providing no user-defined exception message. When no user-defined validation message
 * is provided an opaque validation message is provided.
 */
public class ValidationException extends RuntimeException {
    /**
     * Constructor parametrized with a string instance.
     * @param msg exception message
     */
    public ValidationException(String msg) {
        super(msg);
    }

    /**
     * Constructor parametrized with exception message and any exception, that caused exception.
     * @param msg exception message
     * @param throwable cause
     */
    public ValidationException(String msg, Throwable throwable) {
        super(msg, throwable);
    }
}
