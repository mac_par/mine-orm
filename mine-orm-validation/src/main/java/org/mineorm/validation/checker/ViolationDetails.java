package org.mineorm.validation.checker;

/**
 * {@code ViolationDetails} encloses data for ValidationException.
 */
public final class ViolationDetails {
    private final StringBuilder builder = new StringBuilder();

    private ViolationDetails() {

    }

    /**
     * Create a {@code ViolationDetails} instance.
     * @return an instance
     */
    public static ViolationDetails getInstance() {
        return new ViolationDetails();
    }
    /**
     * Adds message for exception reasons.
     * @param msg message
     */
    public void addViolationDetails(String msg) {
        builder.append(String.format("%s%n", msg));
    }

    /**
     * Clears message.
     */
    public void clear() {
        builder.setLength(0);
    }

    /**
     * Retrieves detailed violation message.
     * @return message
     */
    public String getDetailedMessage() {
        return builder.toString();
    }
}
