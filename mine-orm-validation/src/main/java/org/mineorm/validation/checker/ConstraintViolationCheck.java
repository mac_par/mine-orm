package org.mineorm.validation.checker;

/**
 * Provides basic means for constraint validation on the basis of data provided by annotations.
 * @param <A> type of annotation to be checked
 * @param <S> type of annotation to be checked
 */
public abstract class ConstraintViolationCheck<A, S> {
    private final A annotation;
    private final ViolationDetails violationDetails;

    /**
     * Parametrized constructor with `annotation to be validated.
     * @param checkedAnnotation annotation to be validated.
     */
    public ConstraintViolationCheck(A checkedAnnotation) {
        this.annotation = checkedAnnotation;
        this.violationDetails = ViolationDetails.getInstance();
    }

    /**
     * Validates violation constraints. Basically, when object is null, no violation is raised. Otherwise,
     * it is preferable to raise such a violation. However, it all can depended on constrain f.ex. NotNull raises
     * a constraint violation due to expecting a value, not an empty reference.
     * @param obj object to be checked.
     * @return {@code true} on successfull validation. {@code false} otherwise.
     * @throws org.mineorm.validation.ValidationException in case of any constraint violations
     */
    public abstract boolean validate(S obj);

    /**
     * Retrieves annotation.
     * @return checked annotation
     */
    protected final A getAnnotation() {
        return annotation;
    }

    /**
     * Retrieves violation message.
     * @return detailed violation message
     */
    public final String getViolationMessage() {
        return violationDetails.getDetailedMessage();
    }

    /**
     * Adds violation details.
     * @param msg violation message
     */
    protected void addViolation(String msg) {
        violationDetails.addViolationDetails(msg);
    }

    /**
     * Clears ViolationDetails message.
     */
    protected void clear() {
        violationDetails.clear();
    }
}
