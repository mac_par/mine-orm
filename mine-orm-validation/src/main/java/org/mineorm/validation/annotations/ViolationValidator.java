package org.mineorm.validation.annotations;

import org.mineorm.validation.checker.ConstraintViolationCheck;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotation {@code ViolationValidator} provides class implementation of ConstraintViolationCheck used for
 * checking whether violation has occured.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE, ElementType.FIELD, ElementType.PARAMETER, ElementType.ANNOTATION_TYPE})
@Documented
public @interface ViolationValidator {
    /**
     * {@code ConstraintViolationCheck} class is required, when using this annotation.
     * @return {@code Class} implementing {@code ConstraintViolationCheck}
     */
    Class<? extends ConstraintViolationCheck> value();
}
